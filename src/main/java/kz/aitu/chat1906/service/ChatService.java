package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository chatRepository;

    public void createChat(Chat chat) {
        chatRepository.save(chat);
    }

    public List<Chat> getAll() {
        return chatRepository.findAll();
    }
}
