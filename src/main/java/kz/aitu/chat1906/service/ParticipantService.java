package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParticipantService {
    private UserRepository userRepository;
    private ParticipantRepository participantRepository;

    public List<User> findAllByID(Long chatId) {
        List<Participant> participants =  participantRepository.findParticipantsByChatId(chatId);
        List<User> users = new LinkedList<>();
        for (Participant participant : participants) {
            Optional<User> user = userRepository.findById(participant.getId());
            user.ifPresent(users::add);
        }
        return users;
    }

    public void joinChat(Participant participant) {
        participantRepository.save(participant);
    }
}
