package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private AuthRepository authRepository;

    public String login(String login,String password) {
        Auth auth = authRepository.findAuthByLoginAndPassword(login, password);
        if(auth != null) {
            auth.setLastLoginTimestamp(new Date().getTime());
            auth.setToken(UUID.randomUUID().toString());
            authRepository.save(auth);
            return "logged";
        } else {
            return "Bad!";
        }
    }

    public String identifyToken(String token) {
        Auth auth = authRepository.findAuthByToken(token);
        if(auth != null) {
            return "exists";
        } else {
            return "Bad!";
        }
    }

    /*
    public String register(Auth auth) {
        if(authRepository.findAuthByLogin(auth.getLogin()) != null) {
            auth.setLastLoginTimestamp(new Date().getTime());
            auth.setToken(UUID.randomUUID().toString());
            authRepository.save(auth);
            return "registered";
        } else {
            return "Bad!";
        }
    }*/
}
