package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;

    public void create(Message message) {
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        messageRepository.save(message);
    }

    public void edit(Message message) {
        Message old_message = messageRepository.getOne(message.getId());
        Date date = new Date();
        message.setUpdatedTimestamp(date.getTime());
        message.setCreatedTimestamp(old_message.getCreatedTimestamp());
        messageRepository.save(message);
    }

    public void delete(Long id) {
        messageRepository.deleteById(id);
    }

    public List<Message> getAllById(Participant participant, int count) {
        if(count == -1)
            return messageRepository.getAllByChatId(participant.getChatId());
        else
            return messageRepository.getAllByChatIdLimit(participant.getChatId(), count);
    }

    public List<Message> getAllYours(User user,int count) {
        if(count == -1)
            return messageRepository.getAllByUserId(user.getId());
        else
            return messageRepository.getAllByUserIdLimit(user.getId(), count);
    }

    public void setIsRead(Long userId, Long messageId) throws Exception {
        Optional<Message> optionalMessage = messageRepository.findById(messageId);
        if(optionalMessage.isPresent()) {
            Message message = optionalMessage.get();
            if(message.getUserId().equals(userId)) {
                message.setIsRead(true);
                message.setReadTimestamp(new Date().getTime());
                messageRepository.save(message);
            } else {
                throw new Exception("You are not allowed to change message that is not yours!");
            }
        } else {
            throw new Exception("Message is not found!");
        }
    }

    public List<Message> getAllNotDelivered(Participant participant) throws Exception {
        try {
            /*List<Message> messagesList = messageRepository.getAllByChatId(participant.getChatId());
            messagesList.removeIf(message -> message.getIsDelivered().equals(true));
            return messagesList;*/
            return messageRepository.findByIsDeliveredFalseAndChatId(participant.getChatId());
        } catch (Exception e) {
            throw new Exception("Can't get messages!");
        }

    }
}
