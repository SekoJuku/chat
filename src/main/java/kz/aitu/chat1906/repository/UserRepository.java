package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findById(Integer id);
}

