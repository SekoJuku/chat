package kz.aitu.chat1906.repository;


import kz.aitu.chat1906.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<Auth,Long> {
    public Auth findAuthByLoginAndPassword(String login, String password);
    public Auth findAuthByLogin(String login);
    public Auth findAuthByToken(String token);
}
