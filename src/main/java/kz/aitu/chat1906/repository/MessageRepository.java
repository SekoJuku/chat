package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> getAllByChatId(Long chatId);
    List<Message> getAllByUserId(Long userId);
    List<Message> findByIsDeliveredFalseAndChatId(Long chatId);

    @Query(
            value = "SELECT m FROM Message m WHERE m.chatId = :chatId LIMIT :limit",
            nativeQuery = true)
    List<Message> getAllByChatIdLimit(@Param("chatId") Long chatId, @Param("limit") int limit);

    @Query(
            value = "SELECT m FROM Message m WHERE m.userId = :userId LIMIT :limit",
            nativeQuery = true)
    List<Message> getAllByUserIdLimit(@Param("userId") Long userId, @Param("limit") int limit);

}
