package kz.aitu.chat1906.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private Long chatId;
    private String text;
    @Column(name = "created_timestamp")
    private Long createdTimestamp;
    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_read")
    private Boolean isRead;
    @Column(name = "read_timestamp")
    private Long readTimestamp;
    @Column(name = "is_delivered")
    private Boolean isDelivered;
    @Column(name = "delivered_timestamp")
    private Long deliveredTimestamp;

    @Column(name = "message_type")
    private String messageType;


}
