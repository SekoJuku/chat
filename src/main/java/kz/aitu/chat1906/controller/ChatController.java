package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ChatService;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/chat")
public class ChatController {
    private ChatService chatService;
    private AuthService authService;

    @PostMapping("/create")
    public ResponseEntity<?> createChat(@RequestBody Chat chat, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        chatService.createChat(chat);
        return ResponseEntity.ok("Chat is created!");
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(@RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(chatService.getAll());
    }
}
