package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/login")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login (@RequestParam String login, @RequestParam String password) {
        String response = authService.login(login,password);
        if(response.equals("logged"))
            return ResponseEntity.ok("Logged in!");
        else
            return ResponseEntity.ok("Login or password is not right!");
    }

    /*@PostMapping("/register")
    public ResponseEntity<?> register (@RequestBody Auth auth) {
        String response = authService.register(auth);
        if(response.equals("registered"))
            return ResponseEntity.ok("Registered!");
        else
            return ResponseEntity.ok("Bad request!");
    }*/
}
