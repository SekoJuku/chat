package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private MessageService messageService;
    private AuthService authService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Message message, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        messageService.create(message);
        return ResponseEntity.ok("Message is created!");
    }
    @PutMapping("/edit")
    public ResponseEntity<?> edit(@RequestBody Message message, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        messageService.edit(message);
        return ResponseEntity.ok(message);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        messageService.delete(id);
        return ResponseEntity.ok("Message is deleted!");
    }
    @GetMapping("/showAllMessages")
    public ResponseEntity<?> showAll(@RequestBody Participant participant, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(messageService.getAllById(participant,-1));
    }
    @GetMapping("/get10Messages")
    public ResponseEntity<?> show10(@RequestBody Participant participant, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(messageService.getAllById(participant,10));
    }
    @GetMapping("/showYourMessages")
    public ResponseEntity<?> showYourAll(@RequestBody User user, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(messageService.getAllYours(user,-1));
    }

    @GetMapping("/get10YourMessages")
    public ResponseEntity<?> showYourAll10(@RequestBody User user, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(messageService.getAllYours(user,10));
    }

    @PutMapping("/read/{userId}/{messageId}")
    public ResponseEntity<?> setIsRead(@PathVariable Long userId, @PathVariable Long messageId, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        try {
            messageService.setIsRead(userId,messageId);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Not good!");
        }
        return ResponseEntity.ok("Everything is good!");
    }
    @GetMapping("/getAll/notDelivered")
    public ResponseEntity<?> getAllNotDelivered(@RequestBody Participant participant, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        try {
            return ResponseEntity.ok(messageService.getAllNotDelivered(participant));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Can't get not delivered messages!");
        }
    }
}
