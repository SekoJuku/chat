package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/participant")
public class ParticipantController {
    private ParticipantService participantService;
    private AuthService authService;


    @GetMapping("/joinChat")
    public ResponseEntity<?> joinChatById(@RequestBody Participant participant, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        participantService.joinChat(participant);
        return ResponseEntity.ok("You have joined!");
    }

    @GetMapping("/getParticipants/{chatId}")
    public ResponseEntity<?> getParticipants(@PathVariable Long chatId, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        return ResponseEntity.ok(participantService.findAllByID(chatId));
    }
}
