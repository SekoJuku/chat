package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.UserRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.UserService;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

    private UserService userService;
    private AuthService authService;

    @GetMapping({"", "/"})
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        userService.create(user);
        return ResponseEntity.ok("User is created!");
    }

    @PutMapping("/edit")
    public ResponseEntity<?> edit(@RequestBody User user, @RequestHeader String token) {
        if(!authService.identifyToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        userService.update(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody User user) {
        userService.delete(user);
        return ResponseEntity.ok("User is successfully deleted!");
    }

}
