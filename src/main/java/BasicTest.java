import kz.aitu.chat1906.Chat1906Application;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.UserRepository;
import kz.aitu.chat1906.service.UserService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Chat1906Application.class)
public class BasicTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void checkIfUserExists() {
        User newUser = new User();
        newUser.setName("Ulan");
        User user = userRepository.save(newUser);
        Assertions.assertEquals("Ulan",user.getName());
        userRepository.delete(user);
        //Assertions.assertEquals(1,1);
    }
}
